﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum AbilityStates
{
    idle,
    increase,
    giant,
    decrease,
}

public class AnimatingSprites : MonoBehaviour
{
    public SpriteRenderer spriteRenderer;
    public Sprite[] spriteArray;
    private int contador = 0;
    private int walkDistance = 0;
    private int direccion = 1;
    private float speed;
    private AbilityStates currentState;
    private float timer;
    private float cooldown;



    // Start is called before the first frame update
    void Start()
    {
        cooldown = 0f;
        currentState = AbilityStates.idle;
        Application.targetFrameRate = 12;
        speed = gameObject.GetComponent<DataPlayer>().Speed / (gameObject.GetComponent<DataPlayer>().Weight / 100);
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.flipX = true;
        spriteRenderer.transform.localScale = new Vector3(gameObject.GetComponent<DataPlayer>().Height, gameObject.GetComponent<DataPlayer>().Height, 0);
    }

    // Update is called once per frame1
    void Update()
    {
        ChangingAbilityStates();
        contador++;
        if (contador > 12)
            contador = 1;
        ChangeSprite(contador);
        MovementAnimation();
    }

    void MovementAnimation()
    {
        if (walkDistance == gameObject.GetComponent<DataPlayer>().WalkDistance)
        {
            walkDistance = 0;
            direccion = direccion == 1 ? direccion = -1 : direccion = 1;
            spriteRenderer.flipX = !spriteRenderer.flipX ? spriteRenderer.flipX = true : spriteRenderer.flipX = false;
        }
        spriteRenderer.transform.position = spriteRenderer.transform.position + new Vector3(Input.GetAxis("Horizontal") + direccion * speed * Time.deltaTime, Input.GetAxis("Vertical") * speed * Time.deltaTime, 0);
        walkDistance++;
    }

    void ChangeSprite(int contador)
    {
        switch (contador)
        {
            case 1:
            case 2:
            case 3:
                spriteRenderer.sprite = spriteArray[0];
                break;
            case 4:
            case 5:
            case 6:
                spriteRenderer.sprite = spriteArray[1];
                break;
            case 7:
            case 8:
            case 9:
                spriteRenderer.sprite = spriteArray[2];
                break;
            case 10:
            case 11:
            case 12:
                spriteRenderer.sprite = spriteArray[1];
                break;
        }
    }

    void ChangingAbilityStates()
    {
        switch (currentState)
        {
            case AbilityStates.idle:
                Idleing();
                break;
            case AbilityStates.increase:
                Icreasing();
                break;
            case AbilityStates.giant:
                PowerSpike();
                break;
            case AbilityStates.decrease:
                Decreaseing();
                break;
        }
    }

    void Idleing()
    {
        if (Input.GetKey(KeyCode.Space))
            if (timer + cooldown <= Time.time)
                currentState = AbilityStates.increase;
    }

    void Icreasing()
    {
        spriteRenderer.transform.localScale = spriteRenderer.transform.localScale + new Vector3(0.5f, 0.5f, 0);
        if (gameObject.GetComponent<DataPlayer>().Height + gameObject.GetComponent<DataPlayer>().Height * 2.5 == spriteRenderer.transform.localScale.x)
        {
            currentState = AbilityStates.giant;
            timer = Time.time;
        }
    }
    
    void PowerSpike()
    {
        if (timer + 5 <= Time.time)
            currentState = AbilityStates.decrease;
    }

    void Decreaseing()
    {
        spriteRenderer.transform.localScale = spriteRenderer.transform.localScale - new Vector3(0.5f, 0.5f, 0);
        if (gameObject.GetComponent<DataPlayer>().Height == spriteRenderer.transform.localScale.x)
        {
            currentState = AbilityStates.idle;
            timer = Time.time;
            cooldown = 5f;
        }
    }
}
