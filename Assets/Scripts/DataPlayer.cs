﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PlayerKind
{
    Soccerer,
    Kinght,
    Assassin, 
    Bard,
    Barbarian
}
public class DataPlayer : MonoBehaviour
{
    [SerializeField]
    private string Name;
    public string Surname;
    public float Speed;
    public float Height;
    public float Weight;
    public float WalkDistance;

    public PlayerKind PKind;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log($"Hello {Name}");
        Debug.Log($"Tipus {PKind}, surname {Surname} & speed {Speed}");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
